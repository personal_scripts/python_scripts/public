import argparse
import logging
import requests
import json
import os
import sys
import re

"""
Command line parameter
"""

parser = argparse.ArgumentParser(description='command line options')
parser.add_argument("-b", "--branch", required=False, help="Current Branch name", default="release")
parser.add_argument("-m", "--merge", default=None, required=False, help="Current merge-request")
parser.add_argument("-t", "--tag", default=None, required=False, help="Release tag")

args = parser.parse_args()
project_branch = args.branch
merge_request_id_arg = args.merge
release_tag_arg = args.tag
gitlab_token = os.environ.get('GITLAB_TOKEN')
youtrack_token = os.environ.get('YOUTRACK_TOKEN')
project_id = os.environ.get('PROJECT_ID')
gitlab_url = "url"
youtrack_url = "url"
SSL_CERTIFICATE_PATH = "/chart-tools/rca.crt"

"""
Get latest merge request function
"""

def mr_time_request():
    mr_list = []
    headers = {
        "PRIVATE-TOKEN": gitlab_token
    }
    data = requests.get(f"{gitlab_url}/{project_id}/merge_requests?state=merged", headers=headers, verify=SSL_CERTIFICATE_PATH, timeout=120)
    if data.status_code == 401:
        logging.error("Authorization falied.")
        sys.exit(1)
    elif data.status_code == 404:
        logging.error("Project %s not found or you do not have permissions for project.", project_id)
        sys.exit(1)
    elif data.status_code == 200:
        current_page_number = 1
        while True:
            response = requests.get(f"{gitlab_url}/{project_id}/merge_requests?state=merged", params={'page': current_page_number}, headers=headers, verify=SSL_CERTIFICATE_PATH, timeout=120).json()
            for _, item in enumerate(response):
                mr_mapping = {
                    "target_branch": item["target_branch"],
                    "source_branch": item["source_branch"],
                    "merge_time": item["merged_at"],
                    "merge_id": item["iid"]
                }
                if ((mr_mapping["target_branch"]) == "master") and ((mr_mapping["source_branch"]) == "release"):
                    mr_list.append(mr_mapping)
                    if len(mr_list) == 1:
                        break
            current_page_number += 1
            if len(mr_list) == 1:
                break
        #print(mr_list[0]["merge_time"])
        merge_request_id = (mr_list[0]["merge_id"])
        print(f"Merge request id is {merge_request_id}")
        print("----------------------------------------------------")
        return(merge_request_id)
    else:
        logging.error("Unexpected status code %s", data.status_code)
        sys.exit(1)

"""
Get all commits in merge request
"""

def task_request(merge_request_id):
    commit_list = []
    task_list = []
    regex = r"\w+-\d+"
    headers = {
        "PRIVATE-TOKEN": gitlab_token
    }
    data_tag_request = requests.get(f"{gitlab_url}/{project_id}/merge_requests/{merge_request_id}/commits", headers=headers, verify=SSL_CERTIFICATE_PATH, timeout=120).json()
    for _, item in enumerate(data_tag_request):
        commit_mapping = {
            "commit_title": item["title"],
        }
        test_str = (commit_mapping["commit_title"])
        commit_list.append(test_str)
        matches = re.finditer(regex, test_str, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            task_number = ("{match}".format(match = match.group()))
            task_list.append(task_number)
    
    print("COMMIT LIST:")
    print('\n'.join(commit_list))
    print("----------------------------------------------------")
    uniq_task_list = list(set(task_list))
    print("ISSUE LIST:")
    print('\n'.join(uniq_task_list))
    return(uniq_task_list)

"""
Get latest gitlab tag
"""

def tag_request():
    tag_value = []
    headers = {
        "PRIVATE-TOKEN": gitlab_token
    }
    data_tag_request = requests.get(f"{gitlab_url}/{project_id}/repository/tags?order_by=name&sort=desc", headers=headers, verify=SSL_CERTIFICATE_PATH, timeout=120).json()
    for _, item in enumerate(data_tag_request):
        tag_value = (item["name"])
        break
    return(tag_value)

"""
Set tag in youtrack field
"""

def set_youtrack_field(global_task_list, field_value):
    print("----------------------------------------------------")
    print("FIELD EDITION STATUS:")
    for merge_request_id in global_task_list:
        payload = json.dumps({
          "query": f"Build Version {field_value}",
          "issues": [
            {
              "idReadable": f"{merge_request_id}"
            }
          ]
        })
        headers = {
          'Authorization': f"Bearer {youtrack_token}",
          'Content-Type': 'application/json'
        }

        response = requests.request("POST", youtrack_url, headers=headers, data=payload)
        if response.status_code == 200:
            print(f"✅ the build version field in the issue {merge_request_id} has been changed to {field_value}")
        else:
            print(f"❌ the build version field in the issue {merge_request_id} has not been changed to {field_value}")
            print(response.text)

"""
main function
"""

def main():
    if merge_request_id_arg == None:
        merge_request_id = mr_time_request()
    else:
        print(f"Merge request id is {merge_request_id_arg}")
        merge_request_id = merge_request_id_arg

    global_task_list = task_request(merge_request_id)

    if release_tag_arg == None:
        field_value = tag_request()
    else:
        print(f"release tag is {merge_request_id_arg}")
        field_value = release_tag_arg

    set_youtrack_field(global_task_list, field_value)

if __name__ == "__main__":
    main()
